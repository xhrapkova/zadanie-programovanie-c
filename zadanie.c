#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

int*napln_pole1(int*pole,int n){
	int i;
	
	srand(time(NULL));
	
	for(i=0;i<n;i++)
	{
		pole[i]=rand()%261;
	}
	
	return pole;
}

int*napln_pole2(int*pole1,int*pole2,int n){
	int i;
	
	for(i=0;i<n;i++)
	{
		pole2[i]=pole1[i];
	}
	
	return pole2;
}

void vypis_pole(int*pole,int n){
	int i;
	
	for(i=0;i<n;i++)
	{
		printf("%d  ",pole[i]);
	}
	
	printf("\n\n");
}

void bubble_sort(int*pole,int n,int bubble_porovnania[50],int bubble_vymeny[50],int m){
	int i,j,pomoc,vymien_bubble=0,porovnanie_bubble=0;
	
	for(i=0;i<n;i++)
	{
		for(j=0;j<n-1;j++)
		{
			porovnanie_bubble++;
			if(pole[j]<pole[j+1])
			{
				pomoc=pole[j];
				pole[j]=pole[j+1];
				pole[j+1]=pomoc;
				vymien_bubble++;
			}	
		}
	}
	
	//vypis_pole(pole,n);
	//printf("Pocet vymien bubble sort: %d\n",vymien_bubble);
	//printf("Pocet porovnani bubble sort: %d\n",porovnanie_bubble);
	//printf("\n\n");
	
	bubble_porovnania[m]=porovnanie_bubble;
	bubble_vymeny[m]=vymien_bubble;
}
void shell_sort(int*pole,int n,int shell_porovnania[50],int shell_vymeny[50],int m){
	int i,j,k,pomoc,vymien_shell=0,porovnanie_shell=0;
	
	for(i=n/2;i>0;i=i/2)
	{
		for(j=i;j<n;j++)
		{
			for(k=j-i;k>=0;k=k-i)
			{
			porovnanie_shell++;
			if(pole[k]<pole[k+i])
			{
				pomoc=pole[k];
				pole[k]=pole[k+i];
				pole[k+i]=pomoc;
				vymien_shell++;
			}
		    }
		}
	}
	
	//printf("Pocet vymien shell sort: %d\n",vymien_shell);
	//printf("Pocet porovnani shell sort: %d\n",porovnanie_shell);
	//printf("\n\n");
	
	shell_porovnania[m]=porovnanie_shell;
	shell_vymeny[m]=vymien_shell;
}

void tvorba_svg_porovnania(char*filename,int bubble_porovnania[50],int shell_porovnania[50]){
	int i,q;
	float x;
	FILE*zadanie_porovnania;
	
	zadanie_porovnania=fopen(filename,"w");
	
	fprintf(zadanie_porovnania, "<?xml version=\"1.0\"?>\n");
	fprintf(zadanie_porovnania,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fprintf(zadanie_porovnania, "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox = \"0 0 530 720\" version = \"1.1\">\n");
	
	fprintf(zadanie_porovnania,"<line x1=\"20\" y1=\"700\" x2=\"530\" y2=\"700\" stroke=\"black\" stroke-width=\"2\" />");
	fprintf(zadanie_porovnania,"<line x1=\"20\" y1=\"0\" x2=\"20\" y2=\"700\" stroke=\"black\" stroke-width=\"2\" />");
	fprintf(zadanie_porovnania,"<text x=\"5\" y=\"715\" fill=\"black\">0</text>\n");
	fprintf(zadanie_porovnania,"<text x=\"250\" y=\"40\" fill=\"black\">Porovnania</text>\n");
	fprintf(zadanie_porovnania,"\n<circle cx=\"100\" cy=\"60\" r=\"5\" fill=\"blue\" />");
	fprintf(zadanie_porovnania,"\n<circle cx=\"100\" cy=\"80\" r=\"5\" fill=\"green\" />");
	fprintf(zadanie_porovnania,"<text x=\"120\" y=\"65\" fill=\"black\">Bubble sort</text>\n");
	fprintf(zadanie_porovnania,"<text x=\"120\" y=\"85\" fill=\"black\">Shell sort</text>\n");

	for(i=0;i<50;i++)
	{
		x=20+20+(i*10);
		fprintf(zadanie_porovnania,"\n<circle cx=\"%f\" cy=\"%f\" r=\"5\" fill=\"blue\" />", x,700.0-(bubble_porovnania[i])/100.);
		fprintf(zadanie_porovnania,"\n<circle cx=\"%f\" cy=\"%f\" r=\"5\" fill=\"green\" />", x,700.0-(shell_porovnania[i])/100.);
			
		if (i%4==0){
			fprintf(zadanie_porovnania,"<text x=\"%f\" y=\"715\" fill=\"black\">%.0f</text>\n",x-5,(x-20)/2);
			fprintf(zadanie_porovnania,"<line x1=\"%f\" y1=\"697\" x2=\"%f\" y2=\"703\" stroke=\"black\" stroke-width=\"1\" />",x,x);	
		}
	}

	if (bubble_porovnania[49]>shell_porovnania[49])
		q=bubble_porovnania[49];
	else q=shell_porovnania[49];
	
	i=1;
	while ((i*1000)<=q)
		{
			fprintf(zadanie_porovnania,"<text x=\"25\" y=\"%f\" fill=\"black\">%d</text>\n",(700.0-(i*1000)/100.),(i*1000));
			fprintf(zadanie_porovnania,"<line x1=\"17\" y1=\"%f\" x2=\"23\" y2=\"%f\" stroke=\"black\" stroke-width=\"1\" />",(700.0-(i*1000)/100.),(700.0-(i*1000)/100.));                                         	
			i+=2;}

	fprintf(zadanie_porovnania,"<text x=\"22\" y=\"20\" fill=\"black\" transform=\"rotate(90 10,10)\">pocet porovnani</text>\n");
	fprintf(zadanie_porovnania,"<text x=\"450\" y=\"690\" fill=\"black\">pocet prvkov</text>\n");

	fprintf(zadanie_porovnania, "</svg>\n");
	fclose(zadanie_porovnania);
}

void tvorba_svg_vymeny(char*filename,int bubble_vymeny[50],int shell_vymeny[50]){
	int i,q=0;
	float x;
	FILE*zadanie_vymeny;
	
	zadanie_vymeny=fopen(filename,"w");
	
	fprintf(zadanie_vymeny, "<?xml version=\"1.0\"?>\n");
	fprintf(zadanie_vymeny,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fprintf(zadanie_vymeny, "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox = \"0 0 530 720\" version = \"1.1\">\n");
	
	fprintf(zadanie_vymeny,"<line x1=\"20\" y1=\"700\" x2=\"530\" y2=\"700\" stroke=\"black\" stroke-width=\"2\" />");
	fprintf(zadanie_vymeny,"<line x1=\"20\" y1=\"0\" x2=\"20\" y2=\"700\" stroke=\"black\" stroke-width=\"2\" />");
	fprintf(zadanie_vymeny,"<text x=\"5\" y=\"715\" fill=\"black\">0</text>\n");
	fprintf(zadanie_vymeny,"<text x=\"250\" y=\"40\" fill=\"black\">Vymeny</text>\n");	
	fprintf(zadanie_vymeny,"\n<circle cx=\"100\" cy=\"60\" r=\"5\" fill=\"blue\" />");
	fprintf(zadanie_vymeny,"\n<circle cx=\"100\" cy=\"80\" r=\"5\" fill=\"green\" />");
	fprintf(zadanie_vymeny,"<text x=\"120\" y=\"65\" fill=\"black\">Bubble sort</text>\n");
	fprintf(zadanie_vymeny,"<text x=\"120\" y=\"85\" fill=\"black\">Shell sort</text>\n");




	for(i=0;i<50;i++)
	{
		x=20+20+(i*10);
		fprintf(zadanie_vymeny,"\n<circle cx=\"%f\" cy=\"%f\" r=\"5\" fill=\"blue\" />", x,700.0-(bubble_vymeny[i])/100.);
		fprintf(zadanie_vymeny,"\n<circle cx=\"%f\" cy=\"%f\" r=\"5\" fill=\"green\" />", x,700.0-(shell_vymeny[i])/100.);
		 
		if (i%4==0){
			fprintf(zadanie_vymeny,"<text x=\"%f\" y=\"715\" fill=\"black\">%.0f</text>\n",x-5,(x-20)/2);
			fprintf(zadanie_vymeny,"<line x1=\"%f\" y1=\"697\" x2=\"%f\" y2=\"703\" stroke=\"black\" stroke-width=\"1\" />",x,x);	
		}
	}

	if (bubble_vymeny[49]>shell_vymeny[49])
		q=bubble_vymeny[49];
	else q=shell_vymeny[49];
	
	i=1;
	while ((i*1000)<=q)
		{
			fprintf(zadanie_vymeny,"<text x=\"25\" y=\"%f\" fill=\"black\">%d</text>\n",(700.0-(i*1000)/100.),(i*1000));
			fprintf(zadanie_vymeny,"<line x1=\"17\" y1=\"%f\" x2=\"23\" y2=\"%f\" stroke=\"black\" stroke-width=\"1\" />",(700.0-(i*1000)/100.),(700.0-(i*1000)/100.));                                         	
			i+=2;}

	fprintf(zadanie_vymeny,"<text x=\"22\" y=\"20\" fill=\"black\" transform=\"rotate(90 10,10)\">pocet vymen</text>\n");
	fprintf(zadanie_vymeny,"<text x=\"450\" y=\"680\" fill=\"black\">pocet prvkov</text>\n");

	fprintf(zadanie_vymeny, "</svg>\n");
	fclose(zadanie_vymeny);
}

main(){
	int i;
	int*p1;
	int*p2;
	int bubble_porovnania[50];
	int bubble_vymeny[50];
	int shell_porovnania[50];
	int shell_vymeny[50];
			
	for(i=0;i<50;i++)
	{
		p1=(int*)(malloc((10+(i*5))*sizeof(int)));
		if(p1==NULL)
			return 0;
	
		p2=(int*)(malloc((10+(i*5))*sizeof(int)));
		if(p2==NULL)
			return 0;
		
		p1=napln_pole1(p1,10+(i*5));
		p2=napln_pole2(p1,p2,10+(i*5));
		
		bubble_sort(p1,(10+(i*5)),bubble_porovnania,bubble_vymeny,i);
		shell_sort(p2,(10+(i*5)),shell_porovnania,shell_vymeny,i);
		 	
    }
	
	tvorba_svg_porovnania("zadanie_porovnania.svg",bubble_porovnania,shell_porovnania);	
	tvorba_svg_vymeny("zadanie_vymeny.svg",bubble_vymeny,shell_vymeny);
	
}
